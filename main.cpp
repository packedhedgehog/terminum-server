#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <cppconn/connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using boost::asio::ip::udp;

class server
{
public:
    server(boost::asio::io_service& io_service, short port) : io_service_(io_service), socket_(io_service, udp::endpoint(udp::v4(), port))
	{
		socket_.async_receive_from(boost::asio::buffer(data_, max_length), sender_endpoint_,
								   boost::bind(&server::handle_receive_from, this,
						    	   boost::asio::placeholders::error,
								   boost::asio::placeholders::bytes_transferred));
	}

	void handle_receive_from(const boost::system::error_code& error,
							 size_t bytes_recvd)
	{
		if (!error && bytes_recvd > 0)
		{
			socket_.async_send_to(boost::asio::buffer(data_, bytes_recvd), sender_endpoint_,
									boost::bind(&server::handle_send_to, this,
									boost::asio::placeholders::error,
									boost::asio::placeholders::bytes_transferred));
			std::cout << data_ << '\n';
		}
		else
		{
			socket_.async_receive_from(boost::asio::buffer(data_, max_length), sender_endpoint_,
									   boost::bind(&server::handle_receive_from, this,
									   boost::asio::placeholders::error,
									   boost::asio::placeholders::bytes_transferred));
		}
	}

	void handle_send_to(const boost::system::error_code& /*error*/, size_t /*bytes_sent*/)
	{
		socket_.async_receive_from(boost::asio::buffer(data_, max_length), sender_endpoint_,
								   boost::bind(&server::handle_receive_from, this,
								   boost::asio::placeholders::error,
								   boost::asio::placeholders::bytes_transferred));
	}

	private:
		boost::asio::io_service& io_service_;
		udp::socket socket_;
		udp::endpoint sender_endpoint_;

		enum { max_length = 1024 };
		char data_[max_length];
};

int main(int argc, char* argv[])
{
/*
    using namespace sql;


*/
	try
	{
		if (argc != 2)
		{
			std::cerr << "Usage: terminum_server <port>\n";
			return 1;
		}

		boost::asio::io_service io_service;

		//using namespace std; // For atoi.
        sql::Driver *driver;
        sql::Connection *con;
        sql::Statement *stmt;

        driver = get_driver_instance();
        con = driver->connect("tcp://127.0.0.1:3306/", "root", "sPCxk.QW3er4o,PEL3w0w");
        stmt = con->createStatement();
        stmt->execute("USE TERMINUM");
        stmt->execute("DROP TABLE IF EXISTS test");
        stmt->execute("CREATE TABLE test(id INT, label CHAR(1))");
        stmt->execute("INSERT INTO test(id, label) VALUES (1, 'a')");

        delete stmt;
        delete con;
		std::clog << "Service is running...";
		server s(io_service, std::atoi(argv[1]));


		io_service.run();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

    std::clog << "Service is stopping...";
	return 0;
}

