CC=g++ 
INC_PATH=libraries/mysql-connector/include
CFLAGS=-c -Wall -I$(INC_PATH)
LBITS := $(shell getconf LONG_BIT)
ifeq ($(LBITS),64)
   	LMYSQLLIB=./libraries/mysql-connector/lib64/libmysqlcppconn-static.a
	LIB_PATH=libraries/mysql-connector/lib64
else
   	LMYSQLLIB=./libraries/mysql-connector/lib/libmysqlcppconn-static.a
	LIB_PATH=libraries/mysql-connector/lib
endif
all: server
server: main.o
	$(CC) main.o -o build/terminum_server -lboost_system -L$(LIB_PATH) $(LMYSQLLIB) -lpthread -ldl
main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp 


