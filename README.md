### Instrukcja

*Serwer gry uruchamiamy poprzez komendę:*

```
#!terminal

./terminum_server <port>
```

----------------

Działanie serwera przetestować możemy poprzez program o nazwie client w katalogu "tools/simple udp client/"

*Testowy klient gry uruchamiamy poprzez komendę:*
```
#!terminal

./client <ip> <port> <nrKomendy>

Lub skorzystać z code snippets gdzie w pythonie jest skrypt 

```